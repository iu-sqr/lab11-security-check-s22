# Lab Submission

**Site to test:** https://pikabu.ru
**OWASP scenario:** Forgot password

## Ensure that the time taken for the user response message is uniform

|Test Step|Result|
|-|-|
|Open the website|The pikabu.ru is opened |
|Push "Забыли пароль?" button| Forgot password form is active|
|Enter email of existing account| Done|
|Push "Прислать пароль" button| Website claims it sent an email to restore the pass|
|Save timing from the devtools page| Timing is 680.4 ms|
|Enter email of non-existing account| Done|
|Push "Прислать пароль" button| Website claims it sent an email to restore the pass|
|Save timing from the devtools page| Timing is 110.7 ms|

The test case failed, since for non-existent account the website responded much faster than for existent. Such an attack allows to check if a certain email has an account on the website. This knowledge can be used for social engineering attacks. However, pikabu is just a memes collection, so scamming people for their pikabu account does not lead to any financial profit.


#### Existent account timing
![existent account timing](img/existent_email_timing.jpg)

#### Non-existent account timing
![existent account timing](img/nonexistent_account_timing.jpg)


## Use a side-channel to communicate the method to reset their password

|Test Step|Result|
|-|-|
|Open the website|The pikabu.ru is opened |
|Push "Забыли пароль?" button| Forgot password form is active|
|Enter email of existing account| Done|
|Push "Прислать пароль" button| Website claims it sent an email to restore the pass|
|Verify an email is sent to the specified address| Email is present and contains password reset link|
|Reset the password using sent link| Ok, reset|
|Try to login using new password| Succesful login|

Pikabu communicates reset password link using email which is good, because it additionally verifies account owner's identity.

#### Reset password email
![reset password email](img/reset_password_email.jpg)


## Use URL tokens for the simplest and fastest implementation
As seen above, pikabu sends an URL-token that has to be used to recover the account. The token is 32 alpha-numeric characters. OWASP provides several suggestions to URL Token implementation. I check ones that can be blackbox tested:

#### Ensure tokens are sufficiently long to protect against brute-force attacks
The token is consists of 32 alpha-nums that is $`(33 + 10) ^ {30} \approx 10^{49}`$ possible token combinations. Assuming $10^9$ brute force attempts per seconds$`^{[1]}`$, and a succesfull attack on a key with $N$ possibilities taking $`\frac{N + 1}{2}`$ attepmts takes $`\approx \frac{10^{49}}{2 \cdot 10^9} = 5 \cdot 10^{39} \text{ seconds} \approx 1.58 \cdot 10 ^{30} \text{ centuries}`$ to bruteforce.

Taking into account captcha and rate limiting, bruteforcing the token in infeasible.

Refs:

$`[1]`$: https://nordpass.com/blog/brute-force-attack/ 


#### Ensure tokens are single use and expire after an appropriate period.
Pikabu's password reset links are single-use.
![attempt to repeatedly use the same link does not reset password](img/repeated_password_reset.png)

In black-box testing it is impossible to know exact lifetime of a generated link.

#### Noted security flow
I have requested password reset multiple times. During the testing I have found out, that multiple password reset links are active at the same time. As a result, given a victim requested multiple password reset links and used only one of them, and victim's email account history is exposed to an attacker, the latter can reset password to victim's pikabu account.

|Test Step|Result|
|-|-|
|Open the website|The pikabu.ru is opened |
|Push "Забыли пароль?" button| Forgot password form is active|
|Enter email of existing account| Done|
|Push "Прислать пароль" button| Website claims it sent an email to restore the pass|
|Verify an email is sent to the specified address| Email is present and contains password reset link|
|Refresh the websie|The main page is opened|
|Request another password reset|The website sends a second email with password reset link|
|Go to your email, verify the site sent 2 email with different links|The links are actually different|
|Reset the password using the first link| Password is reset|
|Try to login using new password| Succesful login|
|Reset the password using the second link| Password is reset again|
|Try to login using the latest password| **Succesful login**|
